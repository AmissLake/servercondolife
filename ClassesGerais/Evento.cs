﻿using System;

namespace ClassesGerais
{
    public class Evento
    {
        public int Id { get; set; }
        public DateTime DataHora { get; set; }
        public string Texto { get; set; }
        public string Local { get; set; }
        public string NomeEvento { get; set; }
        public Imagem Imagem { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
    }
}

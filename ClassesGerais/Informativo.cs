﻿using System;
using System.Collections.Generic;

namespace ClassesGerais
{
    public class Informativo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public Usuario Usuario { get; set; }
        public DateTime DataCriacao { get; set; }
        public string Texto { get; set; }
        public IList<Imagem> imagem { get; set; }
        public IList<Comentario> Comentarios { get; set; }
    }
}
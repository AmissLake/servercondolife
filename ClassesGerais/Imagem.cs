﻿namespace ClassesGerais
{
    public class Imagem
    {
        public int Id { get; set; }
        public string ImagemBase64 { get; set; }
        public Informativo Informativo { get; set; }
    }
}
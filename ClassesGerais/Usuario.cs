﻿using System;
using System.Collections.Generic;

namespace ClassesGerais
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public Endereco Endereco { get; set; }
        public string ImagemPerfil { get; set; }
        public bool UsuauarioAtivo { get; set; }
        public string Senha { get; set; }
        public bool Administrador { get; set; }
        public string Telefone { get; set; }
        public IList<Informativo> Informativos { get; set; }
        public IList<Evento> Eventos { get; set; }
        public IList<Comentario> Comentarios { get; set; }
    }
}

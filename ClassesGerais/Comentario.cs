﻿using System;

namespace ClassesGerais
{
    public class Comentario
    {
        public int InformativoId { get; set; }
        public int UsuarioId { get; set; }
        public Informativo Informativo { get; set; }
        public Usuario Usuario { get; set; }
        public string Texto { get; set; }
        public DateTime DataHora { get; set; }
    }
}

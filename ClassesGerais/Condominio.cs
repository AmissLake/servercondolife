﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesGerais
{
    public class Condominio
    {
        public int Id { get; set; }
        public int QuantidadeLicensa { get; set; }
        public string NomeCondominio { get; set; }
        public Endereco EnderecoCondominio { get; set; }
        public string StringConexao { get; set; }
        public int QuantidadeUsuarios { get; set; }
    }
}

﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Class.Bd
{
    public class InformativoDAO : IDisposable
    {
        private UserContext contexto;

        public InformativoDAO()
        {
            this.contexto = new UserContext(); ;
        }

        protected void Adicionar(Informativo informativo)
        {
            informativo.Usuario = contexto.Usuarios.Find(informativo.Usuario.Id);
            contexto.Informativos.Add(informativo);
            contexto.SaveChanges();
        }

        protected void Atualizar(Informativo informativo)
        {
            contexto.Informativos.Update(informativo);
            contexto.SaveChanges();
        }

        public void Dispose()
        {
            contexto.Dispose();
        }

        protected void Remover(Informativo informativo)
        {
            contexto.Informativos.Remove(informativo);
            contexto.SaveChanges();
        }

        protected IList<Informativo> ListarTodos()
        {
            var res = contexto.Informativos
                .Include(img => img.imagem)
                .Include(u => u.Usuario)
                .Select(i =>
                    new Informativo()
                    {
                        Id = i.Id,
                        DataCriacao = i.DataCriacao,
                        Titulo = i.Titulo,
                        Usuario = i.Usuario,
                        Texto = i.Texto,
                        imagem = new List<Imagem>() { i.imagem.First() }
                    })
                .ToList();

            return res;
        }

        protected Informativo InformativoUnico(int Id)
        {
            return contexto.Informativos.Find(Id);
        }

        protected IList<Imagem> ListaImagens(int id)
        {
            return contexto.Imagens.Where(i => i.Informativo.Id == id).ToList();
        }
    }
}

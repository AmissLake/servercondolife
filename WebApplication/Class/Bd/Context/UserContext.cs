﻿using Microsoft.EntityFrameworkCore;
using ClassesGerais;

namespace WebApplication.Class.Bd.Context
{
    public class UserContext : DbContext
    {
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<Imagem> Imagens { get; set; }
        public DbSet<Informativo> Informativos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Condominio> Condominios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comentario>().HasKey(c => new { c.UsuarioId, c.InformativoId });
            modelBuilder.Entity<Informativo>().HasMany(i => i.imagem).WithOne(img => img.Informativo).IsRequired(false).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Usuario>().HasMany(u => u.Eventos).WithOne(e => e.Usuario).OnDelete(DeleteBehavior.Restrict);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Data Source = Amisslake; Initial Catalog = CondoLife; Integrated Security = True");
            //optionsBuilder.UseSqlServer("Data Source = sql-server-db,1433;User Id = SA;Password=Juninhoafram1; Initial Catalog = CondoLife");
            optionsBuilder.UseSqlServer("Server = sqlserver,1433; Initial Catalog = CondoLife;  User ID = SA; Password = Juninhoafram1");
        }
    }
}

﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Class.Bd.Context
{
    public class CondominioContext : DbContext
    {
        public DbSet<Condominio> Condominios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Data Source = Amisslake; Initial Catalog = Condos; Integrated Security = True");
            //optionsBuilder.UseSqlServer("Data Source = sql-server-db,1433;User Id = SA;Password=Juninhoafram1; Initial Catalog = Condos");
            optionsBuilder.UseSqlServer($"Server = sqlserver,1433; Initial Catalog = {Environment.GetEnvironmentVariable("DB_NAME")};  User ID = SA; Password = Juninhoafram1");
        }
    }
}

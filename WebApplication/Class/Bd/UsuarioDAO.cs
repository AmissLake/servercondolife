﻿using ClassesGerais;
using System;
using System.Linq;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Class.Bd
{
    public class UsuarioDAO : IDisposable
    {
        private UserContext contexto;

        public UsuarioDAO()
        {
            this.contexto = new UserContext();
        }

        public void Adicionar(Usuario u)
        {
            contexto.Usuarios.Add(u);
            contexto.SaveChanges();
        }

        public void Atualizar(Usuario u)
        {
            contexto.Usuarios.Update(u);
            contexto.SaveChanges();
        }

        public void Dispose()
        {
            contexto.Dispose();
        }

        public void Remover(Usuario u)
        {
            contexto.Usuarios.Remove(u);
            contexto.SaveChanges();
        }
    }
}

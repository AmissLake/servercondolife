﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Class.Bd
{
    public class EventoDAO : IDisposable
    {
        private UserContext contexto;

        public EventoDAO()
        {
            contexto = new UserContext();
        }

        public void Dispose()
        {
            contexto.Dispose();
        }

        public void Adicionar(Evento evento)
        {
            evento.Usuario = contexto.Usuarios.Find(evento.Usuario.Id);
            evento.UsuarioId = evento.Usuario.Id;
            contexto.Eventos.Add(evento);
            contexto.SaveChanges();
        }

        public void Atualizar(Evento evento)
        {
            contexto.Eventos.Update(evento);
            contexto.SaveChanges();
        }

        public void Remover(int Id)
        {
            contexto.Remove(contexto.Eventos.Find(Id));
            contexto.SaveChanges();
        }
        public Evento Selecionar(int Id)
        {
            return contexto.Eventos.Find(Id);
        }
        public IList<Evento> Listar()
        {
            var res = contexto.Eventos.Include(img => img.Imagem).ToList();
            return res;
        }
    }
}

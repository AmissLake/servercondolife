﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class AtualizaImagemPerfil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Imagens_Usuarios_UsuarioId",
                table: "Imagens");

            migrationBuilder.DropIndex(
                name: "IX_Imagens_UsuarioId",
                table: "Imagens");

            migrationBuilder.DropColumn(
                name: "UsuarioId",
                table: "Imagens");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UsuarioId",
                table: "Imagens",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Imagens_UsuarioId",
                table: "Imagens",
                column: "UsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Imagens_Usuarios_UsuarioId",
                table: "Imagens",
                column: "UsuarioId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

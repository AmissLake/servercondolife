﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class ImagemPerfil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImagemPerfilId",
                table: "Usuarios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_ImagemPerfilId",
                table: "Usuarios",
                column: "ImagemPerfilId");

            migrationBuilder.AddForeignKey(
                name: "FK_Usuarios_Imagens_ImagemPerfilId",
                table: "Usuarios",
                column: "ImagemPerfilId",
                principalTable: "Imagens",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Usuarios_Imagens_ImagemPerfilId",
                table: "Usuarios");

            migrationBuilder.DropIndex(
                name: "IX_Usuarios_ImagemPerfilId",
                table: "Usuarios");

            migrationBuilder.DropColumn(
                name: "ImagemPerfilId",
                table: "Usuarios");
        }
    }
}

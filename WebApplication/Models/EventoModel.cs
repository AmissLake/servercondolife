﻿using ClassesGerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Class.Bd;

namespace WebApplication.Models
{
    public class EventoModel
    {
        public void Inserir(Evento evento)
        {
            using (var contexto = new EventoDAO())
            {
                contexto.Adicionar(evento);
            }
        }

        public IList<Evento> ListaEventos()
        {
            using (var contexto = new EventoDAO())
            {
                return contexto.Listar();
            }
        }
    }
}

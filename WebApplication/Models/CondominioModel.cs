﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Models
{
    public class CondominioModel
    {
        public IList<Condominio> ListarTodos()
        {
            using (var context = new CondominioContext())
            {
                return context.Condominios.ToList();
            }
        }
    }
}
﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Models
{
    public class LoginModel
    {
        public Usuario GetUsuarioCompleto(Login log)
        {
            Usuario usuario;
            using (var context = new UserContext())
            {
                usuario = context.Usuarios
                                .Include(end => end.Endereco)
                                .Where(u => u.Login.Equals(log.login) && u.Senha.Equals(log.senha)).FirstOrDefault() as Usuario;
            }
            return usuario;
        }
    }

    public class Login
    {
        public string login { get; set; }
        public string senha { get; set; }
    }
}

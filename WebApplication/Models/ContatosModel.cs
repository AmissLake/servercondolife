﻿using ClassesGerais;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Class.Bd.Context;

namespace WebApplication.Models
{
    public class ContatosModel
    {
        public IList<Usuario> Contatos()
        {
            IList<Usuario> usuarios;
            using (var context = new UserContext())
            {
                usuarios = context.Usuarios.Include(end => end.Endereco).ToList();
            }
            return usuarios;
        }
    }
}

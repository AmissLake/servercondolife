﻿using ClassesGerais;
using System.Collections.Generic;
using WebApplication.Class.Bd;

namespace WebApplication.Models
{
    public class InformativoModel : InformativoDAO
    {
        public void AdicionarInformativo(Informativo informativo)
        {
            Adicionar(informativo);
        }

        public IList<Informativo> ListarInformativos()
        {
            return ListarTodos();
        }

        public Informativo RetornaInformativo(int Id)
        {
            return InformativoUnico(Id);
        }

        public void EditarInformativo(Informativo informativo)
        {
            Atualizar(informativo);
        }

        public IList<Imagem> ImageList(int id)
        {
            return ListaImagens(id);
        }
    }
}

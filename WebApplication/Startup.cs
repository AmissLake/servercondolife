using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using WebApplication.Class.Bd.Context;

namespace WebApplication
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            using (var contexto = new UserContext())
            {
                contexto.Database.Migrate();
            }
            using (var contexto = new CondominioContext())
            {
                contexto.Database.Migrate();
                if (contexto.Condominios.ToList().Count > 0)
                {
                    contexto.Condominios.Add(new ClassesGerais.Condominio() { NomeCondominio = "Condominio Docker", EnderecoCondominio = new ClassesGerais.Endereco() { Bairro = "Colina", Cidade = "Itatiba", Logradouro = "Alexandrina B Franciscon", Estado = "S�o Paulo", Numero = 180 }, QuantidadeLicensa = 10, QuantidadeUsuarios = 1 });
                    contexto.SaveChanges();
                }
            }
        }
    }
}

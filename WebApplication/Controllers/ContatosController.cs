﻿using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ContatosController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public IList<Usuario> ListaContatos()
        {
            var contatosModel = new ContatosModel();
            try
            {
                var contatos = contatosModel.Contatos();
                if (contatos == null) Response.StatusCode = 204;
                return contatos;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }
    }
}

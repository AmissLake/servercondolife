﻿using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class InformativoController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public void AdicionarInformativo([FromBody]Informativo informativo)
        {
            try
            {
                var adicionar = new InformativoModel();
                adicionar.AdicionarInformativo(informativo);
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
            }
        }

        public Informativo GetInformativo([FromBody]int Id)
        {
            try
            {
                var retorno = new InformativoModel();
                var resp = retorno.RetornaInformativo(Id);
                if (resp == null) Response.StatusCode = 204;
                return resp;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }

        public IList<Informativo> GetTodosInformativos()
        {
            try
            {
                var retorno = new InformativoModel();
                var resp = retorno.ListarInformativos();
                if (resp == null) Response.StatusCode = 204;
                return resp;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }

        public void EditInformativo([FromBody]Informativo informativo)
        {
            try
            {
                var editar = new InformativoModel();
                editar.EditarInformativo(informativo);
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
            }
        }

        public IList<Imagem> GetImageList(int id)
        {
            try
            {
                var model = new InformativoModel();
                var result = model.ImageList(id);
                if (result == null) Response.StatusCode = 204;
                return result;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }
    }
}

﻿using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class EventoController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public void AddEvento([FromBody] Evento evento)
        {
            try
            {
                var model = new EventoModel();
                model.Inserir(evento);
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
            }
        }

        public IList<Evento> GetTodosEventos()
        {
            try
            {
                var model = new EventoModel();
                var resp = model.ListaEventos();
                if (resp == null) Response.StatusCode = 204;
                return resp;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }


        }
    }
}

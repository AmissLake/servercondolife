﻿using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class CondoBaseController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public IList<Condominio> ListAll()
        {
            try
            {
                var retorno = new CondominioModel();
                return retorno.ListarTodos();
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }
    }
}

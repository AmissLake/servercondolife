﻿using Microsoft.AspNetCore.Mvc;
using ClassesGerais;
using System;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LoginController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public Usuario Logar(Login log)
        {
            try
            {
                var model = new LoginModel();
                Usuario usuario = model.GetUsuarioCompleto(log);
                if (usuario == null) Response.StatusCode = 204;
                return usuario;
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
                return null;
            }
        }
    }

}

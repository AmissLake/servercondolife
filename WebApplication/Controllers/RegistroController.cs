﻿using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using System;
using WebApplication.Class.Bd;

namespace WebApplication.Controllers
{
    public class RegistroController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public void Registrar([FromBody]Usuario usuario)
        {
            try
            {
                using (var contexto = new UsuarioDAO())
                {
                    contexto.Adicionar(usuario);
                }
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
            }
        }
    }
}

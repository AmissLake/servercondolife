﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassesGerais;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Class.Bd;

namespace WebApplication.Controllers
{
    public class UsuarioController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public void AtualizaUsuario([FromBody]Usuario usuario)
        {
            try
            {
                using (var contexto = new UsuarioDAO())
                {
                    contexto.Atualizar(usuario);
                }
            }
            catch (Exception e)
            {
                logger.Trace(e);
                Response.StatusCode = 500;
            }
        }
    }
}